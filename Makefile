all: public/good_practices.html \
  public/data_management.html \
  public/github-pandoc.css \
	README.md

public/github-pandoc.css: github-pandoc.css
	cp github-pandoc.css public/github-pandoc.css

public/data_management.html: data_management.tex bibliography.bib  github-pandoc.css
	pandoc data_management.tex --bibliography=bibliography.bib -c github-pandoc.css --citeproc --standalone --number-sections -o public/data_management.html

public/good_practices.html: good_practices.tex bibliography.bib  github-pandoc.css
	pandoc good_practices.tex --bibliography=bibliography.bib -c github-pandoc.css --citeproc --standalone --number-sections -o public/good_practices.html

README.md: good_practices.tex bibliography.bib
	pandoc good_practices.tex --bibliography=bibliography.bib  --citeproc--standalone --number-sections -o README.md
